'use strict'

const config = require('../../config');

const URL_DB = config.URL_DB;
const mongojs = require('mongojs');
var db = mongojs(URL_DB);

const port = process.env.PORT || 3030;
const URL_PROVEEDOR = `https://localhost:${port}/api/hoteles/`;
const MAX_ID_CODE = config.MAX_ID_CODE;
const MIN_ID_CODE = config.MIN_ID_CODE;
const ID_PROVEEDOR = "3333";

const express = require('express');
const logger = require('morgan');
const https = require('https');
const fs = require('fs');
const helmet = require('helmet');
const moment = require('moment');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../../certificates/key.pem'),
    cert: fs.readFileSync('../../certificates/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(helmet());

function filterObj(source, whiteList) {
    const res = {};
    Object.keys(source).forEach((key) => {
      if (whiteList.indexOf(key) !== -1) {
        res[key] = source[key];
      }
    });
    return res;
  }

function idCode() {
    return Math.floor(Math.random() * (MAX_ID_CODE - MIN_ID_CODE) + MIN_ID_CODE);
}

function price(minPrice, maxPrice) {
    const price = (Math.random() * (maxPrice - minPrice) + Number(minPrice));
    return price - (price%0.01);
}


// Iniciamos la escucha
https.createServer(HTTPS_OPTIONS, app)
.listen(port, ()=> {
    console.log(`PROVEEDOR de hoteles ejecutándose en ${URL_PROVEEDOR}`);
});

// Declaramos nuestras rutas y nuestros controladores
app.get('/api/hoteles/', (req, res, next) => {
     
     // Array que sirve para filtrar los parámetros contemplados por
     // nuestra agencia 
     const filterArray = [
       "fechaInicio",
       "fechaFin",
       "numHabitaciones",
       "tipoHabitaciones",
       "ciudad",
       "empresa",
       "estrellas"
     ];

    const queries = req.query;
    console.log(queries);

    let filteredQueries = filterObj(queries, filterArray);

    // Calculo el precio
    if (queries.precioMin && queries.precioMax) {
        filteredQueries.precio = price(queries.precioMin, queries.precioMax);
    } else if (queries.fechaInicio && queries.fechaFin) {
        const diasReserva = moment(queries.fechaFin).diff(queries.fechaInicio)/86400000;
        filteredQueries.precio = 97.13*diasReserva;
    } else if (queries.numHabitaciones) {
        filteredQueries.precio = 63.45*numHabitaciones;
    } else {
        filteredQueries.precio = 321;
    }

    filteredQueries.id = idCode().toString();
    console.log(filteredQueries);

    db.proveedorHoteles.save(filteredQueries, (err, elementoGuardado) => {
        if (err || !elementoGuardado) { throw err; }
        else {
            res.status(200).json({
                result: 'OK',
                queries: filteredQueries
            });
        }
    });
});

// RESERVAR hoteles
app.post('/api/hoteles', (req, res, next) => {
    const id = req.body.id;
    db.proveedorHoteles.findOne({ id: id }, (err, resultado) => {
        if (err || !resultado) {
            console.log(`ERROR al obtener DB.proveedorHoteles.${id}`);
            return next(err);
        }
        res.status(201).json({
            result: 'OK',
            id: id,
            idProveedor: ID_PROVEEDOR,
            caracteristicas: resultado
        });
    });
});


// ELIMINAR RESERVAS DE hoteles
app.delete('/api/hoteles/:id', (req, res, next) => {
    const id = req.params.id;

    res.status(200).json({
        result: 'OK',
        idProveedor: ID_PROVEEDOR,
        id: id
    }); 
});