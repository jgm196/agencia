'use strict'

const config = require('../../config');

const port = process.env.PORT || 3050;
const URL_PROVEEDOR = `https://localhost:${port}/api/pagos`;


const MAX_ID_CODE = config.MAX_ID_CODE;
const MIN_ID_CODE = config.MIN_ID_CODE;

const express = require('express');
const logger = require('morgan');
const https = require('https');
const fs = require('fs');
const helmet = require('helmet');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../../certificates/key.pem'),
    cert: fs.readFileSync('../../certificates/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(helmet());

function idCode() {
    return Math.floor(Math.random() * (MAX_ID_CODE - MIN_ID_CODE) + MIN_ID_CODE);
}

// Iniciamos la escucha
https.createServer(HTTPS_OPTIONS, app)
    .listen(port, () => {
        console.log(`Proveedor de PAGOS (banco) ejecutándose en ${URL_PROVEEDOR}`);
    });


// REALIZAR pagos
app.post('/api/pagos', (req, res, next) => {
    res.status(201).json({
        result: 'OK',
        pago: {
            idPago: idCode().toString(),
            cuentaOrigen: req.body.cuentaOrigen,
            cuentaDestino: req.body.cuentaDestino,
            cantidad: req.body.cantidad
        }
    });
});

// CANCELAR pagos
app.delete('/api/pagos/:idPago', (req, res, next) => {
    res.status(200).json({
        result: 'OK',
    });
});

