'use strict'

const config = require('../config');

const port = process.env.PORT || 3005;
const URL_PAGOS = `https://localhost:${port}/api/pagos`;

const URL_MOD_DB = config.urlDictionary["database"];
const URL_PROVEEDORES = config.URL_PROV_BANCO;

const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const https = require('https');
const fs = require('fs');
const helmet = require('helmet');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../certificates/key.pem'),
    cert: fs.readFileSync('../certificates/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(helmet());

// Iniciamos la escucha
https.createServer(HTTPS_OPTIONS, app)
    .listen(port, () => {
        console.log(`Gestión de PAGOS ejecutándose en ${URL_PAGOS}`);
    });


// OBTENER pagos
app.get('/api/pagos/:idPago', (req, res, next) => {
    const idPago = req.params.idPago;
    console.log(`He recibido el idPago: ${idPago}`);

    fetch(`${URL_MOD_DB}/pagos/${idPago}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha obtenido información del pago: ${idPago}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});


// REALIZAR pagos
app.post('/api/pagos', (req, res, next) => {
    // const cantidad = req.body.cantidad;
    // const cuentaOrigen = req.body.cuentaOrigen;
    // const cuentaDestino = req.body.cuentaDestino;

    // console.log(`Se solicita la reserva para: ${userName}`);
    fetch(`${URL_PROVEEDORES}`, {
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            fetch(`${URL_MOD_DB}/pagos`, {
                method: 'POST',
                body: JSON.stringify(json.pago),
                headers: {
                    'Content-Type': 'application/json'
                    //            'Authorization': TOKEN AGENCIA
                }
            })
                .then(res => res.json())
                .then(json => {
                    res.json(json);
                    console.log(`Se ha efectuado el pago: ${json.object.idPago}`);
                })
                .catch(err => {
                    console.log(err);
                    return res.status(401).json({
                        result: 'KO',
                        mensaje: err
                    });
                });
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

// CANCELAR pagos
app.delete('/api/pagos/:idPago', (req, res, next) => {
    const idPago = req.params.idPago;

    console.log(`Se ha solicitado la cancelación del pago: ${idPago}`);
    fetch(`${URL_PROVEEDORES}/${idPago}`, {
        method: 'DELETE',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
            //            'Authorization': TOKEN AGENCIA
        }
    })
        .then(res => res.json())
        .then(json => {
            console.log(`Pago ${idPago} correctamente cancelado con el proveedor`);
            fetch(`${URL_MOD_DB}/pagos/${idPago}`, {
                method: 'DELETE',
                body: JSON.stringify(req.body),
                headers: {
                    'Content-Type': 'application/json'
                    //            'Authorization': TOKEN AGENCIA
                }
            })
                .then(res => res.json())
                .then(json => {
                    res.json(json);
                    console.log(`Se ha cancelado el pago: ${idPago}`);
                })
                .catch(err => {
                    console.log(err);
                    return res.status(401).json({
                        result: 'KO',
                        mensaje: err
                    });
                });
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

