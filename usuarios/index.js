'use strict'

const config = require('../config');
const SALT_ROUNDS = config.SALT_ROUNDS;

const port = process.env.PORT || 3000;
const URL_WS = `https://localhost:${port}/api/usuarios`;
const URL_MOD_DB = config.urlDictionary["database"] + "/usuarios";

const https = require('https');
const fs = require('fs');

const express = require('express');
const logger = require('morgan');
const bcrypt = require('bcrypt');
const fetch = require('node-fetch');
const servicioToken = require('../servicios/servicioToken');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../certificates/key.pem'),
    cert: fs.readFileSync('../certificates/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Iniciamos la escucha
https.createServer(HTTPS_OPTIONS, app)
    .listen(port, () => {
        console.log(`API REST CRUD SEGURO de USUARIOS ejecutándose en ${URL_WS}`);
    });



// Declaramos nuestras rutas y nuestros controladores

// OBTENER INFORMACIÓN DE UN USUARIO
app.get('/api/usuarios/:userName', (req, res, next) => {
    const user = req.params.userName;
    console.log(`He recibido el username: ${user}`);

    fetch(`${URL_MOD_DB}/${req.params.userName}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha obtenido información del usuario: ${json.object.userName}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});


// CREAR UN USUARIO
app.post('/api/usuarios', (req, res, next) => {
    const nuevoUsuario = req.body;
    // Creo y guardo el hash de la contraaseña
    bcrypt.hash(nuevoUsuario.password, SALT_ROUNDS, (err, hash) => {
        if (err) return next(err);

        nuevoUsuario.password = hash;

        // Guardo el usuario en la BBDD
        fetch(`${URL_MOD_DB}`, {
            method: 'POST',
            body: JSON.stringify(nuevoUsuario),
            headers: {
                'Content-Type': 'application/json'
                //            'Authorization': TOKEN AGENCIA
            }
        })
            .then(res => res.json())
            .then(json => {
                res.json(json);
                console.log(`Se ha creado el usuario: ${json.object.userName}`);
            })
            .catch(err => {
                console.log(err);
                return res.status(401).json({
                    result: 'KO',
                    mensaje: err
                });
            });
    });
});

// MODIFICAR USUARIOS
app.put('/api/usuarios/:userName', (req, res, next) => {
    const userName = req.params.userName;
    const nuevosDatos = req.body;

    fetch(`${URL_MOD_DB}/${userName}`, {
        method: 'PUT',
        body: JSON.stringify(nuevosDatos),
        headers: {
            'Content-Type': 'application/json'
            //            'Authorization': TOKEN AGENCIA
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha modificado información del usuario: ${json.object.userName}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

// ELIMINAR USUARIOS
app.delete('/api/usuarios/:userName', (req, res, next) => {

    fetch(`${URL_MOD_DB}/${req.params.userName}`, {
        method: 'DELETE',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
            //            'Authorization': TOKEN AGENCIA
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha eliminado el usuario: ${json.object.userName}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});


// INICIO DE SESIÓN
app.post('/api/usuarios/login', (req, res, next) => {
    const userName = req.body.userName;
    const givenPassword = req.body.password;

    try {
        fetch(`${URL_MOD_DB}/${req.body.userName}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(json => {
                const usuario = json.object;
                //res.json(json);
                console.log(`Se ha obtenido información del usuario: ${usuario.userName}`);
                bcrypt.compare(givenPassword, usuario.password, (err, correctPassword) => {
                    if (err) return next(err);
                    if (!correctPassword) {
                        console.log(`Inicio de sesión incorrecto del usuario: ${userName}`);
                        res.status(401).json({
                            result: 'KO',
                            mensaje: "Contraseña incorrecta"
                        });
                    } else {
                        console.log(`Inicio de sesión satisfactorio del usuario: ${userName}`);
                        const token = servicioToken.generarToken(usuario);
                        res.status(200).json({
                            result: 'OK',
                            token: token
                        });
                    }
                });
            })
            .catch(err => {
                console.log(err);
                return res.status(401).json({
                    result: 'KO',
                    mensaje: err
                });
            });
    } catch (err) {
        console.log(err);
    }
});
