'use strict'

const config = require('../config');

const PORT = process.env.PORT || 3004;
const URL_MOD_DB = `https://localhost:${PORT}/api/database`;


const URL_DB = config.URL_DB;


const express = require('express');
const logger = require('morgan');
const https = require('https');
const fs = require('fs');
const helmet = require('helmet');
const mongojs = require('mongojs');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../certificates/key.pem'),
    cert: fs.readFileSync('../certificates/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var db = mongojs(URL_DB);
var id = mongojs.ObjectID;

// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(helmet());

// Declaramos estructuras complementarias
const keyColecciones = {
    usuarios: "userName",
    pagos: "idPago",
    coches: "id",
    hoteles: "id",
    vuelos: "id",
    paquetes: "idPaquete"
};


// Iniciamos la escucha
https.createServer(HTTPS_OPTIONS, app)
    .listen(PORT, () => {
        console.log(`Módulo GESTIÓN de BASE DE DATOS ejecutándose en ${URL_MOD_DB}`);
    });

// Declaramos nuestras rutas y nuestros controladores


// OBTENER un elemento
app.get('/api/database/:collection/:identifier', (req, res, next) => {
    const collection = req.params.collection;
    const searchParameter = keyColecciones[collection];
    const identifier = req.params.identifier;
    var query = {};
    query[searchParameter] = identifier;

    console.log(`GET-> Colección: ${collection} || Identificador: ${identifier}`);

    db[collection].findOne(query, (err, resultado) => {
        if (err || !resultado) {
            console.log(`ERROR al obtener DB.${collection}.${identifier}`);
            return next(err);
        }

        console.log(`Se ha obtenido el elemento DB.${collection}.${identifier}: \n ${resultado}`);
        res.status(200).json({
            result: 'OK',
            object: resultado
        });
    });

});

// CREAR un elemento
app.post('/api/database/:collection', (req, res, next) => {
    const collection = req.params.collection;
    const nuevoElemento = req.body;
    console.log(`POST -> Colección: ${collection} || Elemento: \n ${nuevoElemento}`);
    try {
        db[collection].save(nuevoElemento, (err, elementoGuardado) => {
            if (err || !elementoGuardado) { throw err; }
            else {
                console.log(`Se ha creado el elemento: ${elementoGuardado}`);
                res.status(201).json({
                    result: 'OK',
                    object: elementoGuardado
                });
            }
        });
    } catch (err) {
        console.log(`ERROR al guardar el elemento en DB.${collection}`);
        res.status(500).json({
            result: 'KO'
        });
    }
});

// MODIFICAR un elemento
app.put('/api/database/:collection/:identifier', (req, res, next) => {
    const collection = req.params.collection;
    const searchParameter = keyColecciones[collection];
    const identifier = req.params.identifier;
    var query = {};
    query[searchParameter] = identifier;

    const nuevosDatos = req.body;

    db[collection].update(
        query,
        { $set: nuevosDatos },
        { safe: true, multi: false },
        (err, resultado) => {
            if (err) return next(err);

            console.log(`Se ha modificado el elemento: DB.${collection}.${identifier}`);
            res.status(200).json({
                result: 'OK',
                object: resultado
            });
        });
});

// ELIMINAR UN ELEMENTO
app.delete('/api/database/:collection/:identifier', (req, res, next) => {
    const collection = req.params.collection;
    const searchParameter = keyColecciones[collection];
    const identifier = req.params.identifier;
    var query = {};
    query[searchParameter] = identifier;

    db[collection].remove(
        query,
        (err, resultado) => {
            if (err) return next(err);

            console.log(`Se ha eliminado el elemento: DB.${collection}.${identifier}\nTIPO = ${typeof identifier} `);
            res.status(200).json({
                result: 'OK',
                object: resultado
            });
        });
});
