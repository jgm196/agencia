'use strict'

const config = require('../config');

const PORT = process.env.PORT || 3003;
const URL_hoteles = `https://localhost:${PORT}/api/hoteles`;
const URL_PROVEEDORES = "https://localhost:3030/api/hoteles";
const URL_MOD_DB = config.urlDictionary["database"] + "/hoteles";
const URL_USUARIOS = `https://localhost:3000/api/usuarios`
const MAX_ID_CODE = config.MAX_ID_CODE;
const MIN_ID_CODE = config.MAX_ID_CODE;

const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const https = require('https');
const fs = require('fs');
const helmet = require('helmet');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../certificates/key.pem'),
    cert: fs.readFileSync('../certificates/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(helmet());

function idCode() {
    return Math.floor(Math.random() * (MAX_ID_CODE - MIN_ID_CODE) + MIN_ID_CODE);
}

// Iniciamos la escucha
https.createServer(HTTPS_OPTIONS, app)
    .listen(PORT, () => {
        console.log(`Módulo GESTIÓN de hoteles ejecutándose en ${URL_hoteles}`);
    });

// Declaramos nuestras rutas y nuestros controladores

// BUSCAR OFERTAS DE hoteles
app.get('/api/hoteles', (req, res, next) => {
    const qs = req.url.split("?")[1];
    const queryURL = URL_PROVEEDORES + "/?" + qs;
    fetch(`${queryURL}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se han obtenido ofertas de hoteles`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

// RESERVAR hoteles
app.post('/api/hoteles', (req, res, next) => {
    const userName = req.body.userName;
    var idPago;
    console.log(`Se solicita la reserva para: ${userName}`);
    fetch(`${URL_PROVEEDORES}`, {
        method: 'POST',
        body: JSON.stringify({
            'nombre': req.body.completeName,
            'dni': req.body.dni,
            'id': req.body.id
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            json.idPaquete = req.body.idPaquete;
            json.userName = userName;
            json.reservationName = req.body.completeName;
            json.reservationDNI = req.body.dni;
            delete json['result'];

            // Realizo el pago
            fetch(`${config.urlDictionary["pagos"]}`, {
                method: 'POST',
                body: JSON.stringify({
                    cantidad: json.caracteristicas.precio,
                    cuentaOrigen: req.body.bankAccount,
                    cuentaDestino: config.CUENTA_PROVEEDOR
                }),
                headers: {
                    'Content-Type': 'application/json'
                    //            'Authorization': TOKEN AGENCIA
                }
            })
                .then(res => res.json())
                .then(jsonPago => {
                    json.idPago = jsonPago.object.idPago;

                    // Guardo la reserva en la BBDD
                    fetch(`${URL_MOD_DB}`, {
                        method: 'POST',
                        body: JSON.stringify(json),
                        headers: {
                            'Content-Type': 'application/json'
                            //            'Authorization': TOKEN AGENCIA
                        }
                    })
                        .then(res => res.json())
                        .then(json => {
                            console.log(`Se ha efectuado la reserva de hotel con el código: ${json.object.id}, en el paquete ${json.object.idPaquete} para el usuario ${json.object.userName}`);
                            res.status(201).json(json);
                        })
                        .catch(err => {
                            console.log(err);
                            return res.status(401).json({
                                result: 'KO',
                                mensaje: err
                            });
                        });
                })
                .catch(err => {
                    console.log(err);
                    return res.status(401).json({
                        result: 'KO',
                        mensaje: err
                    });
                });
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

// Obtener una reserva efectuada
app.get('/api/hoteles/:id', (req, res, next) => {
    const id = req.params.id;
    fetch(`${URL_MOD_DB}/${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha obtenido información de la reserva: ${json.object.id}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});


// ELIMINAR RESERVAS
app.delete('/api/hoteles/:id', (req, res, next) => {
    const id = req.params.id;
    fetch(`${config.urlDictionary["hoteles"]}/${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            const idPago = json.object.idPago;
            console.log(`Se ha obtenido información de la reserva: ${json.object.id}`);
            fetch(`${URL_PROVEEDORES}/${id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(json => {
                    fetch(`${URL_MOD_DB}/${id}`, {
                        method: 'DELETE',
                        body: JSON.stringify(req.body),
                        headers: {
                            'Content-Type': 'application/json'
                            //            'Authorization': TOKEN AGENCIA
                        }
                    })
                        .then(res => res.json())
                        .then(json => {
                            fetch(`${config.urlDictionary["pagos"]}/${idPago}`, {
                                method: 'DELETE',
                                headers: {
                                    'Content-Type': 'application/json'
                                    //            'Authorization': TOKEN AGENCIA
                                }
                            })

                            res.json(json);
                            console.log(`Se ha eliminado la reserva: ${id}`);
                        })
                        .catch(err => {
                            console.log(err);
                            return res.status(401).json({
                                result: 'KO',
                                mensaje: err
                            });
                        });
                })
                .catch(err => {
                    console.log(err);
                    return res.status(401).json({
                        result: 'KO',
                        mensaje: err
                    });
                });
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});