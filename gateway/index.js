'use strict'

const config = require('../config');

const port = process.env.PORT || 3100;
const URL_GATEWAY = `https://localhost:${port}/api`;

const requireAuth = config.REQUIRE_AUTH;
const urlDictionary = config.urlDictionary;

const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const https = require('https');
const fs = require('fs');
const helmet = require('helmet');
const servicioToken = require('../servicios/servicioToken');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../certificates/key.pem'),
    cert: fs.readFileSync('../certificates/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(helmet());

function auth(req, res, next) {

    if (!requireAuth) { return next(); }

    if (!req.headers.authorization) {
        return res.status(401).json({
            result: 'KO',
            mensaje: "No se ha enviado el token en la cabecera Authorization"
        });
        // return next(new Error("No se encuentra el token en la cabecera"));
    }



    const receivedToken = req.headers.authorization.split(" ")[1];
    servicioToken.verificarToken(receivedToken)
        .then(userName => {
            console.log(`El TOKEN corresponde al usuario ${userName}`);
            if (req.params.collection == "usuarios" && req.params.identifier != null && req.body.userName != null) {
                if (req.params.identifier == userName && req.body.userName == userName) {
                    return next();
                }
            } else if (req.params.identifier == userName || req.body.userName == userName) {
                return next();
            }

            return res.status(401).json({
                result: 'KO',
                mensaje: "El token no es correcto WTF"
            });
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: "El token no es correcto"
            });
        });
}

// Iniciamos la escucha
https.createServer(HTTPS_OPTIONS, app)
    .listen(port, () => {
        console.log(`GATEWAY ejecutándose en ${URL_GATEWAY}`);
    });

// Declaramos nuestras rutas y nuestros controladores

/**
 * USUARIOS
 */

// CREAR USUARIOS
app.post('/api/usuarios', (req, res, next) => {
    const nuevoUsuario = req.body;
    const URL = urlDictionary["usuarios"];

    fetch(`${URL}`, {
        method: 'POST',
        body: JSON.stringify(nuevoUsuario),
        headers: {
            'Content-Type': 'application/json'
            //            'Authorization': TOKEN AGENCIA
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha creado el usuario: ${json.object.userName}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

// INICIO DE SESIÓN
app.post('/api/usuarios/login', (req, res, next) => {
    const userName = req.body.userName;
    const URL = urlDictionary["usuarios"];
    let status;

    fetch(`${URL}/login`, {
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            status = res.status;
            return res.json();
        })
        .then(json => {
            res.status(status).json(json);
            console.log(`Intento de inicio de sesión del usuario: ${userName}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

/**
 * GENERAL
 */

// CREAR un elemento
app.post('/api/:collection', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const collection = req.params.collection;
    const URL = urlDictionary[collection];

    fetch(`${URL}`, {
        method: 'POST',
        body: JSON.stringify(nuevoElemento),
        headers: {
            'Content-Type': 'application/json'
            //            'Authorization': TOKEN AGENCIA
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha creado un elemento en: ${collection}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

// OBTENER un elemento
app.get('/api/:collection/:identifier', auth, (req, res, next) => {
    const collection = req.params.collection;
    const identifier = req.params.identifier;
    const URL = urlDictionary[collection];

    console.log(`IDENTIF:${identifier}`)

    fetch(`${URL}/${identifier}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha obtenido el elemento: ${collection}.${identifier}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

// OBTENER un elemento con DOBLE IDENTIFICADOR 

// OBTENER un elemento con QUERY
app.get('/api/:collection', (req, res, next) => {
    const collection = req.params.collection;
    const BASE_URL = urlDictionary[collection];
    const qs = req.url.split("?")[1];
    const URL = BASE_URL + "/?" + qs;

    fetch(`${URL}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Respuesta obtenida para la query: ${URL}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});


// MODIFICAR un elemento
app.put('/api/:collection/:identifier', auth, (req, res, next) => {
    const collection = req.params.collection;
    const URL = urlDictionary[collection];
    const identifier = req.params.identifier;

    const nuevosDatos = req.body;

    fetch(`${URL}/${identifier}`, {
        method: 'PUT',
        body: JSON.stringify(nuevosDatos),
        headers: {
            'Content-Type': 'application/json'
            //            'Authorization': TOKEN AGENCIA
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha modificado el elemento: ${collection}.${identifier}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});


// TODO: Sólo debería poder eliminarse PAQUETES DESDE EL GATEWAY 


// ELIMINAR un elemento
app.delete('/api/:collection/:identifier', (req, res, next) => {
    const collection = req.params.collection;
    const URL = urlDictionary[collection];
    const identifier = req.params.identifier;

    fetch(`${URL}/${identifier}`, {
        method: 'DELETE',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
            //            'Authorization': TOKEN AGENCIA
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha eliminado el elemento: ${collection}.${identifier}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

// Eliminar un elemento con doble ID
app.delete('/api/:collection/:identifier/:secondIdentifier', (req, res, next) => {
    console.log(`\nHEY ESTOY AQUI\n`);
    const collection = req.params.collection;
    const URL = urlDictionary[collection];
    const identifier = req.params.identifier;
    const secondIdentifier = req.params.secondIdentifier;

    console.log(`\nTORREMOLINOS\n`);
    fetch(`${URL}/${identifier}/${secondIdentifier}`, {
        method: 'DELETE',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
            //            'Authorization': TOKEN AGENCIA
        }
    })
        .then(res => res.json())
        .then(json => {
            console.log(`\nBenalmádena\n`);
            res.json(json);
            console.log(`Se ha eliminado el elemento: ${collection}.${identifier}.${secondIdentifier}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});
