const urlDictionary = {
    usuarios: 'https://localhost:3000/api/usuarios',
    coches : 'https://localhost:3001/api/coches',
    vuelos : 'https://localhost:3002/api/vuelos',
    hoteles : 'https://localhost:3003/api/hoteles',
    database: 'https://localhost:3004/api/database',
    pagos: 'https://localhost:3005/api/pagos',
    paquetes: 'https://localhost:3006/api/paquetes'
}

module.exports = {
    URL_DB: 'mongodb+srv://javi:contra@sd.h6ohw.mongodb.net/SD?retryWrites=true&w=majority',
    URL_PROV_BANCO: 'https://localhost:3050/api/pagos',
    MAX_ID_CODE: 999999,
    MIN_ID_CODE: 100000,
    CUENTA_PROVEEDOR: "ES123456789PROV",
    SECRET: 'askhdbfdsbfaksdbfsbdg',
    // SECRET: 'AAAwEAAQAAAYEA4brWSPQW416Fc6xtR5aFQivkup6FC4JUUhwK+NxVdTotEJTve7R',
    TOKEN_EXP_TIME: 7*24*60, // Una semana en minutos
    SALT_ROUNDS: 10,
    REQUIRE_AUTH: true,
    urlDictionary
}