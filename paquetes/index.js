'use strict'

const config = require('../config');

const port = process.env.PORT || 3006;
const URL_WS = `https://localhost:${port}/api/paquetes`;
const URL_MOD_DB = config.urlDictionary["database"] + "/paquetes";
const MAX_ID_CODE = config.MAX_ID_CODE;
const MIN_ID_CODE = config.MIN_ID_CODE;

const https = require('https');
const fs = require('fs');
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const helmet = require('helmet');

const app = express();
const HTTPS_OPTIONS = {
    key: fs.readFileSync('../certificates/key.pem'),
    cert: fs.readFileSync('../certificates/cert.pem')
}
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(helmet());

function idCode() {
    const result = Math.floor(Math.random() * (MAX_ID_CODE - MIN_ID_CODE) + MIN_ID_CODE);
    console.log(`\n\n RESULTADO ${result}\n`);
    return result;
}

// Iniciamos la escucha
https.createServer(HTTPS_OPTIONS, app)
    .listen(port, () => {
        console.log(`GESTOR DE PAQUETES ejecutándose en ${URL_WS}`);
    });

// Obtener información de un paquete 
app.get('/api/paquetes/:id', (req, res, next) => {
    const id = req.params.id;
    fetch(`${URL_MOD_DB}/${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json(json);
            console.log(`Se ha obtenido información del paquete: ${json.object.id}`);
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

// RESERVAR paquete
app.post('/api/paquetes', (req, res, next) => {
    var paquete = req.body;
    const idPaquete = idCode().toString();
    paquete.idPaquete = idPaquete;
    const paqueteSimple = JSON.parse(JSON.stringify(paquete));
    const userName = req.body.userName;
    
    console.log(`\nID GENERADO: ${idPaquete}`);

    // Obtengo la información del usuario
    fetch(`${config.urlDictionary["usuarios"]}/${userName}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            console.log(`Se ha obtenido el usuario: ${userName}`);
            const usuario = json.object;
            const completeName = usuario.completeName;
            const bankAccount = usuario.bankAccount;
            const dni = usuario.dni;

            for (const servicios in paquete) {
                if (paquete[servicios] instanceof Array) {
                    paquete[servicios].forEach(servicio => {
                        servicio.userName = userName;
                        servicio.completeName = completeName;
                        servicio.dni = dni;
                        servicio.bankAccount = bankAccount;
                        servicio.idPaquete = idPaquete;

                        fetch(`${config.urlDictionary[servicios]}`, {
                            method: 'POST',
                            body: JSON.stringify(servicio),
                            headers: {
                                'Content-Type': 'application/json'
                                //            'Authorization': TOKEN AGENCIA
                            }
                        })
                            .then(console.log(`RESERVADO ${servicios} ${servicio.id} (Paquete: ${idPaquete})`))
                            .catch(err => {
                                console.log(err);
                                return res.status(401).json({
                                    result: 'KO',
                                    mensaje: err
                                });
                            });
                    });
                }
            }
        })
        .then(() => {
            fetch(`${URL_MOD_DB}`, {
                method: 'POST',
                body: JSON.stringify(paqueteSimple),
                headers: {
                    'Content-Type': 'application/json'
                    //            'Authorization': TOKEN AGENCIA
                }
            })
                .then(

                    res.status(201).json({
                        result: 'OK',
                        idPaquete: idPaquete
                    })
                )
                .catch(err => {
                    console.log(err);
                    return res.status(401).json({
                        result: 'KO',
                        mensaje: err
                    });
                });
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});

app.delete('/api/paquetes/:idPaquete', (req, res, next) => {
    const idPaquete = req.params.idPaquete;

    // Obtengo la información del paquete
    fetch(`${config.urlDictionary["paquetes"]}/${idPaquete}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            const paquete = json.object;
            console.log(`Se ha obtenido el paquete: ${idPaquete}`);

            for (const servicios in paquete) {
                if (paquete[servicios] instanceof Array) {
                    paquete[servicios].forEach(servicio => {

                        fetch(`${config.urlDictionary[servicios]}/${servicio.id}`, {
                            method: 'DELETE',
                            headers: {
                                'Content-Type': 'application/json'
                                //            'Authorization': TOKEN AGENCIA
                            }
                        })
                            .then(console.log(`CANCELADO ${servicios} ${servicio.id} (Paquete: ${idPaquete})`))
                            .catch(err => {
                                console.log(err);
                                return res.status(401).json({
                                    result: 'KO',
                                    mensaje: err
                                });
                            });
                    });
                }
            }
        })
        .then(() => {
            fetch(`${URL_MOD_DB}/${idPaquete}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                    //            'Authorization': TOKEN AGENCIA
                }
            })
                .then(

                    res.status(201).json({
                        result: 'OK',
                        idPaquete: idPaquete
                    })
                )
                .catch(err => {
                    console.log(err);
                    return res.status(401).json({
                        result: 'KO',
                        mensaje: err
                    });
                });
        })
        .catch(err => {
            console.log(err);
            return res.status(401).json({
                result: 'KO',
                mensaje: err
            });
        });
});



